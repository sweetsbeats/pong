#include "game.hpp"
#include "state.hpp"
#include <cmath>

// inherits state's constructor
GameState::GameState(sf::RenderWindow *newwindow) : State() {
  // Store the window, for drawing with
  window = newwindow;
  // player paddle
  playerPaddle = sf::RectangleShape(sf::Vector2f(50, 100));
  // AI paddle (copies your actions)
  aiPaddle = sf::RectangleShape(sf::Vector2f(50, 100));
  // our beloved ball
  ball = sf::CircleShape(ballRadius, 16);
  ballVel = sf::Vector2f(-200, -400);
  ball.setPosition(ballPos);

  // GIVE YOUR WORLD COLOUR
  ball.setFillColor(sf::Color::Magenta);
  playerPaddle.setFillColor(sf::Color::Green);
  aiPaddle.setFillColor(sf::Color::Green);
}
void GameState::Event(sf::Event) {}

void GameState::Update(float delta) {
  // Note: High Y values are at bottom of window

  // TODO: Move this line to constructor & on resize
  size = window->getSize();
  // Move ball, remember that delta is the time since last frame
  ball.move(ballVel * delta);
  // ballPos += ballVel * delta;
  // ball.setPosition(ballPos);
  // Bounce off Vertical Min
  if (ballPos.y < 0) {
    // Move positive
    ballVel.y = std::abs(ballVel.y);
  }
  // Bounce off Vertical Max
  if (ballPos.y + 2 * ballRadius > size.y) {
    // Move Negative
    ballVel.y = -std::abs(ballVel.y);
  }
  // Bounce off Horizontal Min
  if (ballPos.x <= 0) {
    // Move positive
    ballVel.x = std::abs(ballVel.x);
  }
  // Bounce off Horizontal Max
  if (ballPos.x + (2 * ballRadius) >= size.x) {
    // Move Negative
    ballVel.x = -std::abs(ballVel.x);
  }

  // Move paddle up, if not at min
  if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up) &&
      playerPaddle.getPosition().y > 0) {
    // up key is pressed: move our character
    playerPaddle.move(0, -playerSpeed * delta);
  }
  // position starts from top left, so we add the height of the paddle to
  // compensate
  // Move paddle down, if not at max
  if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down) &&
      (playerPaddle.getPosition().y + playerPaddle.getSize().y) < size.y) {
    // down key is pressed: move our character
    playerPaddle.move(0, playerSpeed * delta);
  }
}

void GameState::Draw() {
  // draw player's paddle
  window->draw(playerPaddle);

  // TODO: Better AI
  aiPaddle.setPosition(sf::Vector2f(size.x - 50, playerPaddle.getPosition().y));
  // draw ai's paddle
  window->draw(aiPaddle);
  // draw ball
  window->draw(ball);
}
