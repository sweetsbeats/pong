#ifndef TITLE_HPP
#define TITLE_HPP
#include "state.hpp"
#include <SFML/Graphics.hpp>
/**
The Title shown when the game is run.
Needs to cooperate with MainMenuState to reuse the font.
*/
class TitleState : public State {
protected:
  sf::Text titleText;
  sf::Text taglineText;
  sf::Font gameFont;
  sf::Vector2u size;
  sf::RenderWindow *window;

public:
  TitleState(sf::RenderWindow *);

  void Event(sf::Event);

  void Update(float);

  void Draw();
};

#endif
