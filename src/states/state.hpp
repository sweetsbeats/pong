#ifndef STATE_HPP
#define STATE_HPP
#include <SFML/Graphics.hpp>

/** The root of the state tree, and base class for all states.
Mostly just cascades events to the child state
 */
class State {

public:
  State *child;
  State *parent;

  State();

  virtual ~State();

  /// Virtual Empty, for starting things. Doesn't Cascade.
  virtual void Begin();

  /// Switches the child to a different state.
  virtual void GoToState(State *newState);

  /// Cascades events to the children
  virtual void Event(sf::Event event);

  /// Cascades Update call to the children
  virtual void Update(float delta);

  /// Cascades Draw call to the children
  virtual void Draw();
};

#endif
