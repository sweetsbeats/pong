#include "mainmenu.hpp"
#include "game.hpp"
#include "state.hpp"
#include <cmath>

// inherits state's constructor
MainMenuState::MainMenuState(sf::RenderWindow *newwindow) : State() {
  // Store the window, for drawing with
  window = newwindow;
  // TODO: REUSE FONT
  gameFont.loadFromFile("libre.ttf");
  // The text objects in a vector, drawn every frame.
  items = {sf::Text("Human VS Human", gameFont, itemsFontSize),
           sf::Text("Human VS Bot", gameFont, itemsFontSize),
           sf::Text("Bot VS Bot", gameFont, itemsFontSize),
           sf::Text("About", gameFont, itemsFontSize),
           sf::Text("Quit", gameFont, itemsFontSize)};
}
void MainMenuState::Begin() {
  // Set the selected option to the first one, Human VS Human
  active = 0;
}
void MainMenuState::Event(sf::Event event) {
  if (event.type == sf::Event::KeyPressed) {
    switch (event.key.code) {
    case sf::Keyboard::Up:
    case sf::Keyboard::W:
      // Wrap around
      if (active == 0)
        active = items.size();
      // Move up
      active--;
      break;
    case sf::Keyboard::Down:
    case sf::Keyboard::S:
      // Move Down
      active++;
      // Wrap around
      if (active == items.size())
        active = 0;
      break;
    case sf::Keyboard::Space:
    case sf::Keyboard::Return:
    case sf::Keyboard::Right:
    case sf::Keyboard::D:
      // TODO: Do something different for different options
      parent->GoToState(new GameState(window));
      break;
    default:
      // idiot user is pressing random buttons
      break;
    }
  }
}

void MainMenuState::Update(float) {
  // GAMESCREEN SIZE VALUES used for centering text
  sf::Vector2u size = window->getSize();
  unsigned int width = size.x;
  unsigned int height = size.y;

  // TODO: Move this to its own function, and run it only on begin & resize
  // foreach of the items on the menu
  for (unsigned int i = 0; i < items.size(); i++) {
    // Position the option
    items[i].setPosition(
        sf::Vector2f((width - items[i].getGlobalBounds().width) / 2,
                     i * itemsLineHeight + height / 2));
  }
}

void MainMenuState::Draw() {
  // foreach of the items on the menu
  for (unsigned int i = 0; i < items.size(); i++) {
    // TODO: Move the color change to it's own function,
    // and only run on begin & on user input
    // g is the brightness that this item will be if it is inactive
    int g = inactiveColorBaseline +
            inactiveColorGradientSlope * (std::abs(int(i) - int(active)) - 1);
    // set the color
    items[i].setFillColor((i == active) ? colorActive : sf::Color(g, g, g));
    // draw the item
    window->draw(items[i]);
  }
}
