#include "title.hpp"
#include "mainmenu.hpp"
#include "state.hpp"

// inherits state's constructor
TitleState::TitleState(sf::RenderWindow *newwindow) : State() {
  // Store the window, for drawing with
  window = newwindow;
  // TODO: REUSE FONT
  gameFont.loadFromFile("libre.ttf");

  // Big text
  titleText = sf::Text("SUPER COOL PONG GAME", gameFont, 48);
  // Subtext
  taglineText = sf::Text("PRESS SPACE TO BEGIN", gameFont, 36);
}

void TitleState::Event(sf::Event event) {
  if (event.type == sf::Event::KeyPressed) {
    if (event.key.code == sf::Keyboard::Space) {
      // Switch to the main menu when the space bar is hit
      parent->GoToState(new MainMenuState(window));
    }
  }
}
void TitleState::Update(float) {
  // TODO: Move this centering code to it's own function,
  // and only run it on begin & resize
  // GAMESCREEN SIZE VALUES for centering the text
  size = window->getSize();
  unsigned int gameWidth = size.x;
  unsigned int gameHeight = size.y;
  titleText.setPosition(sf::Vector2f(
      (gameWidth - titleText.getGlobalBounds().width) / 2, gameHeight / 2));
  taglineText.setPosition(
      sf::Vector2f((gameWidth - taglineText.getGlobalBounds().width) / 2,
                   (gameHeight + titleText.getGlobalBounds().height + 50) / 2));
}

void TitleState::Draw() {
  window->draw(titleText);
  window->draw(taglineText);
}
