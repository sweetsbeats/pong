#include "state.hpp"
#include <SFML/Graphics.hpp>

State::State() { child = NULL; }

State::~State() {
  if (child != NULL) {
    // Delete child, prevent memory leaks
    delete child;
  }
}

void State::Begin() {}

void State::GoToState(State *newState) {
  if (child != NULL) {
    // Delete child, prevent memory leaks
    delete child;
  }
  child = newState;
  child->parent = this;
  if (child != NULL) {
    // Doesn't cascade
    child->Begin();
  }
}

void State::Event(sf::Event event) {
  if (child != NULL) {
    // Cascade
    child->Event(event);
  }
}

void State::Update(float delta) {
  if (child != NULL) {
    // Cascade
    child->Update(delta);
  }
}

void State::Draw() {
  if (child != NULL) {
    // Cascade
    child->Draw();
  }
}
