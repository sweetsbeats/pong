#ifndef MAINMENU_HPP
#define MAINMENU_HPP
#include "state.hpp"
#include <SFML/Graphics.hpp>
/**
A Main Menu. Needs to run different things depending on the option selected,
and also needs to cooperate with TitleState to reuse the font.
*/
class MainMenuState : public State {
protected:
  sf::Font gameFont;
  std::vector<sf::Text> items;
  sf::RenderWindow *window;
  // the currently selected item
  unsigned int active;

  // the item's.. font.. size...
  const float itemsFontSize = 36;
  // the spacing inbetween the items
  const float itemsLineHeight = 50;
  // the color for the active item
  const sf::Color colorActive = sf::Color::Red;
  // Base brightness
  const int inactiveColorBaseline = 234;
  // change in brightness for every item farther from the active item
  const int inactiveColorGradientSlope = -56;

public:
  MainMenuState(sf::RenderWindow *);

  void Begin();

  void Event(sf::Event);

  void Update(float);

  void Draw();
};

#endif
