#ifndef GAME_HPP
#define GAME_HPP
#include "state.hpp"
#include <SFML/Graphics.hpp>
/**
The Game's world, eventually maybe containing an ECS. (I'm kidding)
*/
class GameState : public State {
protected:
  sf::RectangleShape playerPaddle;
  sf::RectangleShape aiPaddle;
  sf::CircleShape ball;
  sf::Vector2u size;
  sf::Vector2f ballPos;
  sf::Vector2f ballVel;
  const float ballRadius = 15;
  const float playerSpeed = 400;
  sf::RenderWindow *window;

public:
  GameState(sf::RenderWindow *);

  void Event(sf::Event);

  void Update(float);

  void Draw();
};

#endif
