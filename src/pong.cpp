#include <SFML/Graphics.hpp>
#include <SFML/System/Clock.hpp>

#include <SFML/Graphics/Font.hpp>
#include <SFML/Graphics/Text.hpp>

#include "states/state.hpp"
#include "states/title.hpp"

int main() {
  // What we want, not what we'll get
  const int screenWidth = 800;
  const int screenHeight = 600;

  // style::default includes style::resizable
  sf::RenderWindow window(sf::VideoMode(screenWidth, screenHeight),
                          "SUPER COOL PONG GAME", sf::Style::Default);

  window.setVerticalSyncEnabled(true);

  // The root of the state stack
  State rootState = State();
  // Add a titlestate onto the root state
  rootState.GoToState(new TitleState(&window));

  // A Global view, that is just the full window.
  sf::View view = window.getView();

  // Used for calculating FPS, and timestep
  sf::Clock Clock;
  while (window.isOpen()) {
    sf::Event event;
    while (window.pollEvent(event)) {
      // Close the window
      if (event.type == sf::Event::Closed) {
        window.close();
      }

      // Maintain the view, so that it always contains the entire window.
      if (event.type == sf::Event::Resized) {
        view.setSize(event.size.width, event.size.height);
        view.setCenter(event.size.width / 2, event.size.height / 2);
      }
      // Send the event cascading up the state stack
      rootState.Event(event);
    }
    // Time since last frame
    float delta = Clock.getElapsedTime().asSeconds();
    Clock.restart();
    // Only update game if window has focus.
    if (window.hasFocus()) {
      // each state tells the next state to update.
      rootState.Update(delta);
    }

    window.clear();
    // each state tells the next state to render.
    rootState.Draw();
    // Flip
    window.display();
  }
  return 0;
}
